using InputSystem;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Camera _camera;
    [SerializeField] private Vector2 _minMaxZoom;
    [SerializeField] private float _zoomSpeed;
    [SerializeField] private float _moveSpeed;
    [SerializeField] private InputProvider _inputProvider;

    private IInput _input;
    private float _initialZoom;
    private Vector3 _initialPosition;

    private void Awake()
    {
        _input = _inputProvider.GetInput();
        _initialZoom = _camera.orthographicSize;
        _initialPosition = transform.position;
    }

    private void OnEnable()
    {
        _input.Zoom += OnZoom;
        _input.Reset += OnReset;
    }
    
    private void OnDisable()
    {
        _input.Zoom -= OnZoom;
        _input.Reset -= OnReset;
    }

    private void Update()
    {
        Vector3 axis = _input.Axis;
        if (axis.sqrMagnitude > 0)
        {
            Vector3 moveVector = new Vector3(axis.x, 0, axis.y);
            transform.position += moveVector * _moveSpeed;
        }
    }

    private void OnZoom(float delta)
    {
        float orthographicSize = _camera.orthographicSize;
        orthographicSize = Mathf.Clamp(orthographicSize + delta * _zoomSpeed, _minMaxZoom.x, _minMaxZoom.y);
        _camera.orthographicSize = orthographicSize;
    }
    
    private void OnReset()
    {
        _camera.orthographicSize = _initialZoom;
        transform.position = _initialPosition;
    }
}
