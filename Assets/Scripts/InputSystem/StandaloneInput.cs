using System;
using UnityEngine;

namespace InputSystem
{
    public class StandaloneInput : MonoBehaviour, IInput
    {
        public Vector2 Axis { get; private set; }
        public event IInput.ZoomDelegate Zoom;
        public event Action Reset;

        private bool _mousePressed;
        private Vector3 _prevMousePosition;

        public void Update()
        {
            UpdateMove();
            UpdateZoom();
            UpdateReset();
        }

        private void UpdateZoom()
        {
            float scrollWheel = Input.GetAxis("Mouse ScrollWheel");
            if (scrollWheel != 0)
            {
                Zoom?.Invoke(scrollWheel * -1);
            }
        }

        private void UpdateMove()
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                _prevMousePosition = Input.mousePosition;
                _mousePressed = true;
            }

            if (_mousePressed)
            {
                Vector3 mousePosition = Input.mousePosition;
                Vector3 delta = _prevMousePosition - mousePosition;
                Axis = delta * -1;
                _prevMousePosition = mousePosition;
            }

            if (Input.GetKeyUp(KeyCode.Mouse0))
            {
                _mousePressed = false;
                Axis = Vector2.zero;
            }
        }

        private void UpdateReset()
        {
            if (Input.GetKey(KeyCode.Mouse1))
            {
                Reset?.Invoke();
            }
        }
    }
}