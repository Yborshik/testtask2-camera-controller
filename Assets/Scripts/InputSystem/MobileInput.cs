using System;
using UnityEngine;

namespace InputSystem
{
    public class MobileInput : MonoBehaviour, IInput
    {
        [SerializeField] private float _doubleTapDelta = 0.3f;

        private float _nextDoubleTapTime;
        
        public Vector2 Axis { get; private set; }
        public event IInput.ZoomDelegate Zoom;
        public event Action Reset;

        private void Update()
        {
            UpdateMove();
            UpdateZoom();
            UpdateReset();
        }

        private void UpdateMove()
        {
            if (Input.touchCount == 1)
            {
                Touch touch = Input.GetTouch(0);
                Axis = touch.deltaPosition * -1;
            }
            else
            {
                Axis = Vector2.zero;
            }
        }

        private void UpdateZoom()
        {
            if (Input.touchCount == 2)
            {
                Touch touch0 = Input.GetTouch(0);
                Touch touch1 = Input.GetTouch(1);

                Vector2 touch0PrevPos = touch0.position - touch0.deltaPosition;
                Vector2 touch1PrevPos = touch1.position - touch1.deltaPosition;

                float prevMagnitude = (touch0PrevPos - touch1PrevPos).magnitude;
                float currentMagnitude = (touch0.position - touch1.position).magnitude;

                float delta = currentMagnitude - prevMagnitude;
                
                Zoom?.Invoke(delta * -1);
            }
        }

        private void UpdateReset()
        {
            if (Input.touchCount == 1)
            {
                Touch touch = Input.GetTouch(0);

                if (touch.phase == TouchPhase.Began && Time.time < _nextDoubleTapTime)
                {
                    Reset?.Invoke();
                }

                _nextDoubleTapTime = Time.time + _doubleTapDelta;
            }
        }
    }
}