using System;
using UnityEngine;

namespace InputSystem
{
    public interface IInput
    {
        Vector2 Axis { get; }
        event ZoomDelegate Zoom;
        event Action Reset;

        public delegate void ZoomDelegate(float delta);
    }
}