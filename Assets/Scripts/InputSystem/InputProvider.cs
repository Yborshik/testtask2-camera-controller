using UnityEngine;

namespace InputSystem
{
    public class InputProvider : MonoBehaviour
    {
        [SerializeField] private StandaloneInput _standaloneInput;
        [SerializeField] private MobileInput _mobileInput;

        private IInput _input;
        
        private void Awake()
        {
            InitializeInput();
        }

        public IInput GetInput()
        {
            return _input;
        }

        private void InitializeInput()
        {
#if UNITY_EDITOR || UNITY_STANDALONE
            _input = Instantiate(_standaloneInput);
#else
            _input = Instantiate(_mobileInput);
#endif
        }
    }
}